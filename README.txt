CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration


INTRODUCTION
------------

BREW TOOLS
.........................

Current Maintainer: Darren Whittington <darren.whittington@gmail.com>

This module adds 3 new block types to the blocks administration page:
 * Calculates ABV
 * Calculates Hop Utilisation
 * Calculates Strike Temperature
 * Temperature conversion
 * Plato / Gravity conversion

INSTALLATION
------------

The module should be installed by decompressing the archive into your contrib 
folder or using the user interface in Drupal.

CONFIGURATION
-------------

Once install the user can go to the Blocks page and add blocks to specified 
pages as required, blocks can be styles using usual methods.

There are specific configuration settings to adjust values used for specific 
blocks.
